"use strict";


// EJERCICIO REPASO DIA 3:

class Banco {
  constructor(nombre, direccion) {
    this.nombre = nombre;
    this.direccion = direccion;
    this.clientes = [];
  }
}

class CuentaBancaria {
  constructor(id) {
    this.balance = 0;
    this.id = id;
  }
}

class Titular {
  constructor(nombre, genero, monedero) {
    this.nombre = nombre;
    this.genero = genero;
    this.monedero = monedero;
    this.id = Math.floor(Math.random() * 999999999);
  }

  abrirCuentaBancaria(banco) {
    /* 1. Generar un ID para la nueva cuenta bancaria.*/

    const generarId = Math.floor(Math.random() * 9999999999);
    // console.log(generarId);

    /* 2. Acceder al array "clientes" del banco y almacenar el
       nombre y el ID de la cuenta bancaria del nuevo cliente.*/

    banco.clientes.push({ nombre: this.nombre, id: generarId });
    // console.log(banco.clientes);

    /* 3. Crear una nueva cuenta bancaria a la que debemos pasar
          el nuevo ID: 
          creo instancia de la clase CuentaBancaria:*/

    return new CuentaBancaria(generarId);
  }

  ingresarDinero(cantidad, cuenta) {
    /* 1. Comprobar si en el monedero tenemos la cantidad que
        deseamos ingresar. De no ser asi, mostrar un mensaje
        que diga que no tenemos suficiente dinero en el
        monedero.
       2. Si tenemos suficiente dinero en el monedero, solo
        queda restar en el monedero la cantidad que vamos
        a ingresar.
       3. Acceder a la propiedad "balance" de la cuenta bancaria
        y sumar la cantidad a ingresar. Mostrar un mensaje de
        que el ingreso ha sido realizado.
       
        Hago un if que revise qué condición se cumple y que ejecute
        las órdenes indicadas en el enunciado. */

    if (this.monedero < cantidad) {
      return `No tiene suficiente dinero en el monedero para hacer este ingreso.`;
    } else {
      this.monedero = this.monedero - cantidad;
      cuenta.balance = cuenta.balance + cantidad;
      return `Ha ingresado ${cantidad} euros en su cuenta. El saldo actual de su cuenta es de ${cuenta.balance} euros.`;
    }
  }

  retirarDinero(cantidad, cuenta) {
    /* 1. Comprobar si en la propiedad "balance" de nuestra
        cuenta tenemos la cantidad que deseamos retirar.
        De no ser asi­, mostrar un mensaje que diga que
        no tenemos suficiente dinero en la cuenta.
       2. Si tenemos suficiente dinero en la cuenta, solo
        queda restar en el balance la cantidad que vamos
        a retirar.
       3. Acceder a la propiedad "monedero" del titular
        y sumar la cantidad retirada al monedero. Mostrar
        un mensaje de que el ingreso ha sido realizado.

        Hago otro if que revise qué condición se cumple y que ejecute
        las órdenes indicadas en el enunciado. */

    if (cuenta.balance < cantidad) {
      return `No tiene suficiente saldo en cuenta para hacer esta retirada.`;
    } else {
      cuenta.balance = cuenta.balance - cantidad;
      this.monedero = this.monedero + cantidad;
      return `Ha retirado ${cantidad} euros de su cuenta. El saldo actual de su cuenta es de ${cuenta.balance} euros. El ingreso de ${cantidad} euros ha sido realizado en su monedero.`;
    }
  }

  mostrarSaldo(cuenta) {
    /* 1. Acceder a la propiedad "balance" de la cuenta y
          mostrar un mensaje que nos indique nuestro saldo
          actual.*/

    return `El saldo actual de su cuenta es de ${cuenta.balance} euros.`;
  }
}

/* 1. Crear instancia de la class Titular (el id del titular se genera
aleatoriamente):*/
const manolo = new Titular("Manolo", "hombre", 1200);
console.log(manolo);

/* 2. Crear instancia de la class Banco:*/
const bancoUno = new Banco("BancoSantander", "Linares Rivas, 6. A Coruña");
console.log(bancoUno);

/* 3. Abrir cuenta bancaria para la instancia de la class Titular (el id
de la cuenta también se genera aleatoriamente):*/
const cuentaManolo = manolo.abrirCuentaBancaria(bancoUno);
console.log(cuentaManolo);

/*4. Ingresar dinero en la cuenta bancaria creada:

4.1. Si hay saldo suficiente en el monedero:*/
const ingresoManoloConSaldo = manolo.ingresarDinero(400, cuentaManolo);
console.log(ingresoManoloConSaldo);
/* Ahora Manolo tiene 400 euros en el balance de su cuenta:*/
console.log(cuentaManolo.balance);
/* Y le quedan 1200-400 = 800 euros en el monedero:*/
const saldoMonederoManolo = `Manolo tiene ${manolo.monedero} euros en su monedero`;
console.log(saldoMonederoManolo);
console.log(manolo.monedero);

/* 4.2. Si no hay saldo suficiente en el monedero:*/
const ingresoManoloSinSaldo = manolo.ingresarDinero(1500, cuentaManolo);
console.log(ingresoManoloSinSaldo);

/*5. Mostrar saldo de la cuenta bancaria: (para ver saldo al crear 
  la cuenta,comentar el apartado 4.1 */
const saldoCuentaManoloUno = manolo.mostrarSaldo(cuentaManolo);
console.log(saldoCuentaManoloUno);
console.log(cuentaManolo.balance);

/*6. Retirar dinero en la cuenta bancaria creada:

6.1. Si tenemos saldo suficiente en la cuenta:*/

const retiradaManoloConSaldo = manolo.retirarDinero(150, cuentaManolo);
console.log(retiradaManoloConSaldo);
/* Ahora Manolo tiene 400 - 150 = 250 euros en el balance de su cuenta:*/
console.log(cuentaManolo.balance);
const saldoCuentaManoloDos = manolo.mostrarSaldo(cuentaManolo);
console.log(saldoCuentaManoloDos);
/* Y en el monedero tiene: 800 + 150 = 950 euros.*/
const saldoMonederoManoloDos = `Manolo tiene ${manolo.monedero} euros en su monedero`;
console.log(saldoMonederoManoloDos);
console.log(manolo.monedero);

/* 6.2. Si no tenemos saldo suficiente en la cuenta:*/
const retiradaManoloSinSaldo = manolo.retirarDinero(600, cuentaManolo);
console.log(retiradaManoloSinSaldo);
